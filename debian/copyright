Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: bcron
Upstream-Contact: Bruce Guenter <bruceg@em.ca>
Source: https://untroubled.org/bcron/archive

Files: *
Copyright: 2005 Bruce Guenter <bruceg@em.ca>
License: GPL-2+

Files: crontab.5
Copyright: 1988,1990,1993,1994 Paul Vixie
           2005 Bruce Guenter
           2004 Internet Systems Consortium, Inc. ("ISC")
           1997,2000 Internet Software Consortium, Inc.
License: ISC

Files: debian/*
Copyright: 2016-2018 Dmitry Bogatov <KAction@gnu.org>
           2005-2014 Gerrit Pape <pape@smarden.org>
License: GPL-2+

License: GPL-2+
 This program is free software; you can redistribute it and/or
 modify it under the terms of the GNU General Public License
 as published by the Free Software Foundation; either version 2
 of the License, or (at your option) any later version.
 .
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 .
 You should have received a copy of the GNU General Public License
 along with this program. If not, see <http://www.gnu.org/licenses/>.
 .
 On Debian system, copy of GNU Lesser General Public License version 2
 is also located at `/usr/share/common-licenses/GPL-2'

License: ISC
 Permission to use, copy, modify, and distribute this software for any
 purpose with or without fee is hereby granted, provided that the above
 copyright notice and this permission notice appear in all copies.
 .
 THIS SOFTWARE IS PROVIDED ``AS IS'' AND WITHOUT ANY EXPRESS OR IMPLIED
 WARRANTIES, INCLUDING, WITHOUT LIMITATION, THE IMPLIED WARRANTIES OF
 MERCHANTIBILITY AND FITNESS FOR A PARTICULAR PURPOSE. THE AUTHORS AND
 CONTRIBUTORS ACCEPT NO RESPONSIBILITY IN ANY CONCEIVABLE MANNER.
